# RobotArm
## A better example program for cheap chinese robotic arms

This repository has an Arduino-sketch for the cheap RC servo controlled robotic arms from various Chinese resellers (e.g. [this one](https://www.sinoning.cc/product-page/snarm)).

The sketch has following features:

+ Manual control mode
+ Calibration mode to set servo ranges
+ Automatic path setup
+ Automatic path replay
+ All settings are stored in the non-volatile EEPROM

To use the different modes, follow the instructions given by the sketch in the Arduino Serial Monitor (default baud rate: 9600)