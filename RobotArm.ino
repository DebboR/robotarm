#include "VarSpeedServo.h"
#include <EEPROM.h>

// Define path speed
#define PATH_SPEED 25

// Define serial baud rate
#define SERIAL_BAUD 9600

// Define potentiometer pins
#define POT_1 0
#define POT_2 1
#define POT_3 2
#define POT_4 3

// Define servo pins
#define SERV_1 11
#define SERV_2 10
#define SERV_3 9
#define SERV_4 6

// Define EEPROM addresses
#define EEPROM_SERV_1_MIN 0
#define EEPROM_SERV_1_MAX 4
#define EEPROM_SERV_2_MIN 8
#define EEPROM_SERV_2_MAX 12
#define EEPROM_SERV_3_MIN 16
#define EEPROM_SERV_3_MAX 20
#define EEPROM_SERV_4_MIN 24
#define EEPROM_SERV_4_MAX 28
#define EEPROM_PATH_LENGTH 32
#define EEPROM_PATH_POINTS 33

// Define possible modes
#define MODE_MANUAL 0
#define MODE_CALIBRATE 1
#define MODE_PATH_SETUP 2
#define MODE_PATH_FOLLOW 3

// Types
typedef struct pointStr {
    int serv1Pos = 0;
    int serv2Pos = 0;
    int serv3Pos = 0;
    int serv4Pos = 0;
} point;

// Servo objects
VarSpeedServo servo1;
VarSpeedServo servo2;
VarSpeedServo servo3;
VarSpeedServo servo4;

// Global variables
byte mode = MODE_MANUAL;
int posServo1 = 0;
int posServo2 = 0;
int posServo3 = 0;
int posServo4 = 0;

int serv1Min, serv1Max;
int serv2Min, serv2Max;
int serv3Min, serv3Max;
int serv4Min, serv4Max;

char pathLength = 0;
point path[100];

// Helper functions
void waitOnCharacter(){
    while(Serial.available() != 0) Serial.read();
    while(Serial.available() < 1){}
    Serial.read();
}

void freeMoveUntilCharacter(bool useBounds = 0){
    while(Serial.available() != 0) Serial.read();
    while(Serial.available() == 0){
        posServo1 = map(analogRead(POT_1), 0, 1023, useBounds ? serv1Min : 0, useBounds ? serv1Max : 180);
        servo1.write(posServo1);
        posServo2 = map(analogRead(POT_2), 0, 1023, useBounds ? serv2Min : 0, useBounds ? serv2Max : 180);
        servo2.write(posServo2);
        posServo3 = map(analogRead(POT_3), 0, 1023, useBounds ? serv3Min : 0, useBounds ? serv3Max : 180);
        servo3.write(posServo3);
        posServo4 = map(analogRead(POT_4), 0, 1023, useBounds ? serv4Min : 0, useBounds ? serv4Max : 180);
        servo4.write(posServo4);
    }
}

void printCommands() {
    Serial.println("--------------------------");
    Serial.println("-------- RobotArm --------");
    Serial.println("--- Robbe Derks - 2018 ---");
    Serial.println("--------------------------");
    Serial.println("Serial commands:");
    Serial.println("  1: Manual mode");
    Serial.println("  2: Calibrate end stops");
    Serial.println("  3: Automatic path following setup");
    Serial.println("  4: Automatic path following");
}

void handleSerialInput(){
    char input = Serial.read();
    switch(input){
        case '1':
            mode = MODE_MANUAL;
            break;
        case '2':
            mode = MODE_CALIBRATE;
            break;
        case '3':
            mode = MODE_PATH_SETUP;
            break;
        case '4':
            mode = MODE_PATH_FOLLOW;
            break;
        default:
            break;
    }
}

void readServoExtrema() {
    EEPROM.get(EEPROM_SERV_1_MIN, serv1Min);
    EEPROM.get(EEPROM_SERV_1_MAX, serv1Max);
    EEPROM.get(EEPROM_SERV_2_MIN, serv2Min);
    EEPROM.get(EEPROM_SERV_2_MAX, serv2Max);
    EEPROM.get(EEPROM_SERV_3_MIN, serv3Min);
    EEPROM.get(EEPROM_SERV_3_MAX, serv3Max);
    EEPROM.get(EEPROM_SERV_4_MIN, serv4Min);
    EEPROM.get(EEPROM_SERV_4_MAX, serv4Max);
}

// Mode handlers
void modeManual() {
    posServo1 = map(analogRead(POT_1), 0, 1023, serv1Min, serv1Max);
    servo1.write(posServo1);
    posServo2 = map(analogRead(POT_2), 0, 1023, serv2Min, serv2Max);
    servo2.write(posServo2);
    posServo3 = map(analogRead(POT_3), 0, 1023, serv3Min, serv3Max);
    servo3.write(posServo3);
    posServo4 = map(analogRead(POT_4), 0, 1023, serv4Min, serv4Max);
    servo4.write(posServo4);
}

void modeCalibrate() {
    Serial.println("");
    Serial.println("--- Calibration mode ---");
    Serial.println("Move potentiometers to a known safe value (e.g. 50%) and send any character");

    waitOnCharacter();

    Serial.println("Move safely to minimum values for all pots and send any character");
    freeMoveUntilCharacter(false);
    EEPROM.put(EEPROM_SERV_1_MIN, posServo1);
    EEPROM.put(EEPROM_SERV_2_MIN, posServo2);
    EEPROM.put(EEPROM_SERV_3_MIN, posServo3);
    EEPROM.put(EEPROM_SERV_4_MIN, posServo4);

    Serial.println("Move safely to maximum values for all pots and send any character");
    freeMoveUntilCharacter(false);
    EEPROM.put(EEPROM_SERV_1_MAX, posServo1);
    EEPROM.put(EEPROM_SERV_2_MAX, posServo2);
    EEPROM.put(EEPROM_SERV_3_MAX, posServo3);
    EEPROM.put(EEPROM_SERV_4_MAX, posServo4);

    Serial.println("Calibration succesfull. Going back to manual mode");
    readServoExtrema();
    mode = MODE_MANUAL;
}

void modePathSetup(){
    Serial.println("");
    Serial.println("--- Automatic path setup mode ---");
    Serial.println("Move arm into desired checkpoint positions (maximum 100 positions). Send:");
    Serial.println("  1: Save intermediate point");
    Serial.println("  2: Save last point");

    char pointIndex = 0;
    do {
        freeMoveUntilCharacter(true);

        path[pointIndex].serv1Pos = posServo1;
        path[pointIndex].serv2Pos = posServo2;
        path[pointIndex].serv3Pos = posServo3;
        path[pointIndex].serv4Pos = posServo4;

        pointIndex++;

        Serial.println(" Point saved!");
        delay(500);
    } while(Serial.read() != '2');
    pathLength = pointIndex;

    EEPROM.put(EEPROM_PATH_LENGTH, pathLength);
    EEPROM.put(EEPROM_PATH_POINTS, path);
    Serial.println("Path saved!");

    mode = MODE_MANUAL;
}

void modePathFollow(){
    Serial.println("");
    Serial.println("--- Automatic path follow mode ---");
    Serial.println("Follows stored path until a character is sent");
    
    char pathIndex = 0;
    while(Serial.available() != 0) Serial.read();
    while(Serial.available() == 0){
        servo1.write(path[pathIndex].serv1Pos, PATH_SPEED);
        servo2.write(path[pathIndex].serv2Pos, PATH_SPEED);
        servo3.write(path[pathIndex].serv3Pos, PATH_SPEED);
        servo4.write(path[pathIndex].serv4Pos, PATH_SPEED);
        while(servo1.isMoving() || servo2.isMoving() || servo3.isMoving() || servo4.isMoving()) delay(10);
        pathIndex++;
        pathIndex %= pathLength;
        Serial.println("Got to point!");
    }
}

// Arduino functions
void setup() {
    // Setup servos
    servo1.attach(SERV_1);
    servo2.attach(SERV_2);
    servo3.attach(SERV_3);
    servo4.attach(SERV_4);

    // Setup serial connection
    Serial.begin(SERIAL_BAUD);
    printCommands();

    // Setup extrema
    readServoExtrema();

    // Setup path
    EEPROM.get(EEPROM_PATH_LENGTH, pathLength);
    EEPROM.get(EEPROM_PATH_POINTS, path);
}

void loop() {
    handleSerialInput();
    switch(mode){
        case MODE_MANUAL:
            modeManual();
            break;
        case MODE_CALIBRATE:
            modeCalibrate();
            break;
        case MODE_PATH_SETUP:
            modePathSetup();
            break;
        case MODE_PATH_FOLLOW:
            modePathFollow();
            break;
        default:
            break;
    }
}
